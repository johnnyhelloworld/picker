const colleague = [
    [
        "Abdelaziz",
        "https://ca.slack-edge.com/T3LKMEULS-U050ZUTBATZ-ed28f28e278d-512",
    ],
    [
        "Ahmed",
        "https://ca.slack-edge.com/T3LKMEULS-U05K4L41NJK-a400d46742a9-512",
    ],
    [
        "Amine",
        "https://ca.slack-edge.com/T3LKMEULS-U03L36N6NN6-6d1a6fba9ad0-512",
    ],
    [
        "Anaïs",
        "https://ca.slack-edge.com/T3LKMEULS-U011ALSDY8P-38c85ea9deb2-512",
    ],
    [
        "Aymen",
        "https://ca.slack-edge.com/T3LKMEULS-U04MGS1A0AW-cea3680f82d8-512",
    ],
    [
        "Christopher",
        "https://ca.slack-edge.com/T3LKMEULS-U02GR8APXEC-907ec20941c7-512",
    ],
    [
        "Ghazi",
        "https://ca.slack-edge.com/T3LKMEULS-U05ATU7PN7N-ebc1c049e330-512",
    ],
    [
        "Hassan",
        "https://ca.slack-edge.com/T3LKMEULS-U05AUG855MX-582bdfca1d04-512",
    ],
    [
        "Hedi",
        "https://ca.slack-edge.com/T3LKMEULS-U0517SPUZPG-5f4f04f2b896-512",
    ],
    [
        "Hichem",
        "https://ca.slack-edge.com/T3LKMEULS-U05A97JPSRL-7ea5ed16a87f-512",
    ],
    [
        "Ines",
        "https://ca.slack-edge.com/T3LKMEULS-U038WGYJDQT-cce6044f712f-512",
    ],
    [
        "Johnny",
        "https://ca.slack-edge.com/T3LKMEULS-U052X123UCU-fd94959c3360-512",
    ],
    [
        "Kevin",
        "https://ca.slack-edge.com/T3LKMEULS-U02PFPD0CUB-2ba5177ef663-512",
    ],
    [
        "Khalil",
        "https://ca.slack-edge.com/T3LKMEULS-U03F9PL1XDE-c22de1869c5c-512",
    ],
    [
        "Mathias",
        "https://ca.slack-edge.com/T3LKMEULS-U0167GX6HBR-aecf8d19e154-512",
    ],
    [
        "Rachid",
        "https://ca.slack-edge.com/T3LKMEULS-U03E5DDQ2A3-77090c3c71e5-512",
    ],
    [
        "Walid",
        "https://ca.slack-edge.com/T3LKMEULS-U01K621M2MA-795f933b8e72-512",
    ],
];

const alreadyPicked = [];
const listAlreadyPicked = document.querySelector(".alreadyPicked");
const btnGenerate = document.querySelector("button");
const btnBefore = document.querySelector(".btn-before");
const btnNext = document.querySelector(".btn-next");

function generateColleague() {
    if (alreadyPicked.length === colleague.length) {
        selectRandomPickedColleague();
        return null;
    }

    const div = document.querySelector(".colleague");
    const name = document.querySelector(".colleague-name");

    const random = Math.floor(Math.random() * colleague.length);

    if (checkIfAlreadyPicked(colleague[random][0])) {
        generateColleague();
    } else {
        div.style.backgroundImage = `url(${colleague[random][1]})`;
        div.style.backgroundSize = "contain";
        div.style.backgroundPosition = "center";
        updateName(colleague[random][0]);
        alreadyPicked.push(colleague[random][0]);
    }
}

function selectRandomPickedColleague() {
    const random = Math.floor(Math.random() * alreadyPicked.length);
    const div = document.querySelector(".colleague");
    div.style.backgroundImage = `url(${colleague[random][1]})`;
    div.style.backgroundSize = "contain";
    div.style.backgroundPosition = "center";
    updateName(colleague[random][0]);
}

function updateName(colleague) {
    const name = document.querySelector(".colleague-name");
    name.innerHTML = colleague;
}

function appendAlreadyPicked() {
    const div = document.querySelector(".alreadyPicked");

    const index = colleague.findIndex(
        (e) => e[0] === alreadyPicked[alreadyPicked.length - 1]
    );

    const btn = document.createElement("button");
    btn.innerHTML = alreadyPicked[alreadyPicked.length - 1];

    const span = document.createElement("span");
    span.innerHTML = alreadyPicked.length;

    const div2 = document.createElement("div");

    btn.addEventListener("click", () => {
        const div = document.querySelector(".colleague");
        div.style.backgroundImage = `url(${colleague[index][1]})`;
        updateName(colleague[index][0]);
    });

    div2.append(span);
    div2.appendChild(btn);

    div.appendChild(div2);
}

function checkIfAlreadyPicked(name) {
    if (alreadyPicked.includes(name)) {
        return true;
    } else {
        return false;
    }
}

function updateCounter() {
    const counter = document.querySelector(".counter");
    counter.innerHTML = alreadyPicked.length;
}

btnGenerate.addEventListener("click", () => {
if (generateColleague() !== null) {
    appendAlreadyPicked();
    updateCounter();
}

if(alreadyPicked.length === colleague.length){
    const btn = document.querySelector(".generate");
    btn.innerHTML = "Select random picked colleague";
}
});

document.addEventListener("keydown", (e) => {
    if (e.keyCode === 32 || e.keyCode === 13) {
        btnGenerate.click();
    }
});